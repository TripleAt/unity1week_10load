# unity1week 10道

↓公開先<br>
[unityroom 1週間ゲームジャム 10道](https://unityroom.com/games/10road)

↓イベントページ<br>
[Unity 1週間ゲームジャム お題「10」](https://unityroom.com/unity1weeks/11)

様々なアセットなどを使いたかったため制作。<br>
Unity1Weekというゲームジャムに参加したときのもの。<br>
企画、曲作成、モデル準備なども含めて34時間半で作成

### 以下のツール、アセットを初使用<br>
[Cinemachine](https://assetstore.unity.com/packages/essentials/cinemachine-79898)<br>
[MagicaVoxel](https://ephtracy.github.io/)<br>
[Mixamo](https://www.mixamo.com/)<br>
[UniRx](https://assetstore.unity.com/packages/tools/integration/unirx-reactive-extensions-for-unity-17276)<br>
[Zenject](https://assetstore.unity.com/packages/tools/integration/zenject-dependency-injection-ioc-17758)<br>
Text Mesh Pro


### 作成したときのブログ
[Unity 1週間ゲームジャムに初参加したって話](https://orotiyamatano.hatenablog.com/entry/2018/11/29/Unity_1%E9%80%B1%E9%96%93%E3%82%B2%E3%83%BC%E3%83%A0%E3%82%B8%E3%83%A3%E3%83%A0%E3%81%AB%E5%88%9D%E5%8F%82%E5%8A%A0%E3%81%97%E3%81%9F%E3%81%A3%E3%81%A6%E8%A9%B1)
