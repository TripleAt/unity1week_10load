﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using HC.AI;
using Zenject;

public class GameManager : MonoBehaviour {
    private bool tryGame = true;
    private StateMachine fms;
    [Inject]
    private SoundManager sound;


    // Start is called before the first frame update
    void Start () {
        fms = GetComponent<StateMachine>();
        sound.LoadBgm("kitaero","BGM/kitaero");
        sound.LoadBgm("end","BGM/end");
        sound.LoadSe("decision","SE/decision");
        sound.LoadSe("magic1","SE/magic-cure");
        sound.LoadSe("magic2","SE/magic-statusup");
        sound.LoadSe("punch","SE/punch-middle");
    }

    public void StateChange(){
        switch(fms.CurrentState){
            case TitleState titleState:     //タイトルなら
                fms.Transition<MainState>();    //ゲームメインへ
            break;
            case MainState mainState:       //ゲームメインなら
                fms.Transition<ResultState>();  //リザルトへ
            break;
            case ResultState resultState:   //リザルト画面なら
                fms.Transition<TitleState>();    //初期化処理へ
            break;
        }
        
    }


}