﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// サウンド管理
public class SoundManager :MonoBehaviour {

    /// SEチャンネル数
    const int SE_CHANNEL = 4;

    /// サウンド種別
    enum eType {
        Bgm, // BGM
        Se, // SE
    }


    // サウンド再生のためのゲームオブジェクト
    GameObject _object = null;
    // サウンドリソース
    AudioSource _sourceBgm = null; // BGM
    AudioSource _sourceSeDefault = null; // SE (デフォルト)
    AudioSource[] _sourceSeArray; // SE (チャンネル)
    // BGMにアクセスするためのテーブル
    Dictionary<string, _Data> _poolBgm = new Dictionary<string, _Data> ();
    // SEにアクセスするためのテーブル 
    Dictionary<string, _Data> _poolSe = new Dictionary<string, _Data> ();

    /// 保持するデータ
    class _Data {
        /// アクセス用のキー
        public string Key;
        /// リソース名
        public string ResName;
        /// AudioClip
        public AudioClip Clip;

        /// コンストラクタ
        public _Data (string key, string res) {
            Key = key;
            ResName = "Sounds/" + res;
            //Debug.Log(ResName);
            // AudioClipの取得
            Clip = Resources.Load (ResName) as AudioClip;
            Debug.Log(Clip);
        }
    }

    void Awake(){
        _sourceSeArray = new AudioSource[SE_CHANNEL];
    }


    /// AudioSourceを取得する
    AudioSource _GetAudioSource (eType type, int channel = -1) {
        if (_object == null) {
            // GameObjectがなければ作る
            _object = new GameObject ("Sound");
            // 破棄しないようにする
            GameObject.DontDestroyOnLoad (_object);
            // AudioSourceを作成
            _sourceBgm = _object.AddComponent<AudioSource> ();
            _sourceSeDefault = _object.AddComponent<AudioSource> ();
            for (int i = 0; i < SE_CHANNEL; i++) {
                _sourceSeArray[i] = _object.AddComponent<AudioSource> ();
            }
        }

        if (type == eType.Bgm) {
            // BGM
            return _sourceBgm;
        } else {
            // SE
            if (0 <= channel && channel < SE_CHANNEL) {
                // チャンネル指定
                return _sourceSeArray[channel];
            } else {
                // デフォルト
                return _sourceSeDefault;
            }
        }
    }

    // サウンドのロード
    // ※Resources/Soundsフォルダに配置すること
    public void LoadBgm (string key, string resName) {
        if (_poolBgm.ContainsKey (key)) {
            // すでに登録済みなのでいったん消す
            _poolBgm.Remove (key);
        }
        _poolBgm.Add (key, new _Data (key, resName));
    }
    public void LoadSe (string key, string resName) {
        if (_poolSe.ContainsKey (key)) {
            // すでに登録済みなのでいったん消す
            _poolSe.Remove (key);
        }
        _poolSe.Add (key, new _Data (key, resName));
    }

    /// BGMの再生
    /// ※事前にLoadBgmでロードしておくこと
    public AudioSource PlayBgm (string key) {
        if (_poolBgm.ContainsKey (key) == false) {
            // 対応するキーがない
            return null;
        }

        // いったん止める
        this.StopBgm ();

        // リソースの取得
        var _data = _poolBgm[key];

        // 再生
        var source = _GetAudioSource (eType.Bgm);
        source.loop = true;
        source.clip = _data.Clip;
        source.Play ();

        return source;
    }
    /// BGMの停止
    public bool StopBgm () {
        _GetAudioSource (eType.Bgm).Stop ();

        return true;
    }

    /// SEの再生
    /// ※事前にLoadSeでロードしておくこと
    public AudioSource PlaySe (string key, int channel = -1) {
        if (_poolSe.ContainsKey (key) == false) {
            // 対応するキーがない
            return null;
        }
        AudioSource source;

        // リソースの取得
        var _data = _poolSe[key];

        if (0 <= channel && channel < SE_CHANNEL) {
            // チャンネル指定
            source = _GetAudioSource (eType.Se, channel);
            source.clip = _data.Clip;
            source.Play ();
        } else {
            // デフォルトで再生
            source = _GetAudioSource (eType.Se);
            source.PlayOneShot (_data.Clip);
        }

        return source;
    }
}