﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using HC.AI;
using Zenject;
using UniRx.Triggers;

[DisallowMultipleComponent]
public class ResultState : State
{
    [Inject]
    GameManager gm;

    [SerializeField]
    private GameObject resultObj;
    [SerializeField]
    private GameObject reTryButtonObj;
    [SerializeField]
    private GameObject tweetButtonObj;
    [Inject]
    private Score score;
    [Inject]
    private SoundManager sound;
    [SerializeField]
    private Transform gaugeListTrans;
    [SerializeField]
    private GameObject character;

    
    #region event

    private void Start()
    {
        BeginStream.Subscribe(_ => beginStream());
        //UpdateStream.Subscribe(_ => updateStream()).AddTo(this);
        // FixedUpdateStream.Subscribe(_ => Debug.Log("Result State FixedUpdate"));
        // LateUpdateStream.Subscribe(_ => Debug.Log("Result State LateUpdate"));
        EndStream.Subscribe(_ => endStream());
        // OnDrawGizmosStream.Subscribe(_ => Debug.Log("Result State OnDrawGizmos"));
        // OnGUIStream.Subscribe(_ => Debug.Log("Result State OnGUI"));
    }

    #endregion

    private void beginStream(){
        Vector3 v;
        v = character.transform.position;
        v.y = -0.51f;
        character.transform.position = v;
        AudioSource audio =sound.PlayBgm("end");
        audio.volume = 0.6f;
        Debug.Log("Result State Bigin");
        active(true);
        var tweetButton = tweetButtonObj.GetComponent<Button>();
        var reTryButton = reTryButtonObj.GetComponent<Button>();

        //ツイートボタン
        tweetButton.onClick.AsObservable().Subscribe(_ => {
            sound.PlaySe("decision");
            naichilab.UnityRoomTweet.TweetWithImage ("10road", "あなたは"+ score.getScore() +"鍛えました！", "unityroom", "unity1week");
        });

        //リトライボタン
        reTryButton.onClick.AsObservable().Subscribe(_ => {
            sound.PlaySe("decision");
            Observable.Timer (TimeSpan.FromSeconds (1)).Subscribe (l => {
                GameRetry ();
            });
        });
    }

    private void endStream(){
        Debug.Log("Result State End");
        active(false);
    }



    private void active(bool b){
        tweetButtonObj.SetActive(b);
        reTryButtonObj.SetActive(b);
        resultObj.SetActive(b);
    }
    
    void GameRetry () {
        
        foreach (Transform n in gaugeListTrans) {
            GameObject.Destroy (n.gameObject);      //片付け
        }
        gm.StateChange();
    }
}