﻿using System;
using UnityEngine;
using UniRx;
using HC.AI;
using UniRx.Triggers;
using Zenject;


[DisallowMultipleComponent]
public class TitleState : State
{
    [Inject]
    GameManager gm;
    
    [SerializeField]
    private GameObject startStr;
    [SerializeField]
    private GameObject titleStr;
    [SerializeField]
    private GameObject titleObj;
    private IDisposable _disposable;

    private ObservableEventTrigger touch;
    [Inject]
    private SoundManager sound;
    [SerializeField]
    private GameObject character;

    #region event

    private void Start()
    {
        touch = gameObject.GetComponent<ObservableEventTrigger>();
        BeginStream.Subscribe(_ => beginStream());
        //UpdateStream.Subscribe().AddTo(this);
        // FixedUpdateStream.Subscribe(_ => Debug.Log("Title State FixedUpdate"));
        // LateUpdateStream.Subscribe(_ => Debug.Log("Title State LateUpdate"));
        EndStream.Subscribe(_ => endStream()).AddTo(this);
        // OnDrawGizmosStream.Subscribe(_ => Debug.Log("Title State OnDrawGizmos"));
        // OnGUIStream.Subscribe(_ => Debug.Log("Title State OnGUI"));
    }

    #endregion

    private void beginStream(){
        AudioSource audio = sound.PlayBgm("kitaero");
        character.transform.position = new Vector3(0,-0.51f,-7);
        character.transform.rotation = Quaternion.Euler(0,180,0);
        character.transform.localScale = new Vector3(20,20,20);
        audio.volume = 0.95f;
        Debug.Log("Title State Bigin");
        active(true);
        var mouseDownStream = touch.OnPointerClickAsObservable().Take(1);

        _disposable = mouseDownStream.Subscribe (_ => {
            GameStart ();
            Observable.Timer (TimeSpan.FromSeconds (1)).Subscribe (l => {
                gm.StateChange();
            }).AddTo(this);
        });
    }

    private void endStream(){
        Debug.Log("Title State End");
        active(false);
        _disposable.Dispose();
    }

    private void active(bool b){
        startStr.SetActive(b);
        titleStr.SetActive(b);
        titleObj.SetActive(b);
    }
    
    void GameStart () {
        sound.PlaySe("decision");
        //SE再生とアニメーション
    }

    private void updateStream(){

    }
}