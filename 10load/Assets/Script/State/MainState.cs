﻿using System;
using HC.AI;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Zenject;
using Cinemachine;

[DisallowMultipleComponent]
public class MainState : State {
    [Inject]
    GameManager gm;
    private IDisposable _disposable;
    [SerializeField]
    private GameObject mainObj;
    [SerializeField]
    private GameObject DescriptionObj;
    [SerializeField]
    private Transform gaugeListTrans;
    [SerializeField]
    private GameObject gaugeResetObj;
    [SerializeField]
    private CountDown count;
    [Inject]
    private Score score;
    [SerializeField]
    private Gauge gauge;
    [SerializeField]
    private TextMeshProUGUI t;
    private ObservableEventTrigger touch;
    [Inject]
    private SoundManager sound;
    [SerializeField]
    private Animator anime;
    [SerializeField]
    private GameObject character;
    [SerializeField]
    private CinemachineVirtualCamera cam;

    #region event

    private void Start () {
        touch = gameObject.GetComponent<ObservableEventTrigger> ();
        BeginStream.Subscribe (_ => beginStream ());
        UpdateStream.Subscribe ().AddTo (this);
        EndStream.Subscribe (_ => endStream ());
    }

    #endregion

    private void beginStream () {
        Vector3 v = gaugeListTrans.localPosition;
        v.y = 235.00f;
        gaugeListTrans.localPosition = v;
        int num = 0;
        score.clearScore ();
        Debug.Log ("Main State Bigin");
        active (true);
        var mouseDownStream = touch.OnPointerClickAsObservable ();
        var cnt = Observable.Timer (TimeSpan.FromSeconds (0), TimeSpan.FromSeconds (1)).Take (4);

        cnt.Subscribe (t => {
            if (t < 3) {
                count.bigin ((3 - (int) t).ToString ());
            } else {
                count.bigin ("GO");
                GameStart ();
                _disposable = mouseDownStream.Take (10).Subscribe (_ => {
                    Debug.Log (num);
                    scoreAdd (num++);
                });
            }
        });

    }

    private void endStream () {
        Debug.Log ("Main State End");
        active (false);
        _disposable.Dispose ();
    }

    private void active (bool b) {
        mainObj.SetActive (b);
        DescriptionObj.SetActive (b);
    }

    void GameStart () {
        //Sound再生とアニメーション
        DescriptionObj.SetActive (false);
        anime.SetTrigger("doSiups");
        Vector3 v = character.transform.position;
        v.y = -0.3f;
        character.transform.position = v;
    }

    void scoreAdd (int num) {
        sound.PlaySe ("punch");
        float gaugeNum = gauge.getGaugeNum;
        score.addScore (gaugeNum);
        Debug.Log (gaugeNum);
        gauge.gaugeReset ();
        
        GameObject g = Instantiate (gaugeResetObj, gaugeListTrans);
        g.GetComponent<ResultGauge>().gaugeNum = gaugeNum;
        Vector3 v;
        switch(num){
            case 3:
                v = character.transform.position;
                v.y = -0.4f;
                character.transform.position = v;
                cam.enabled = false;
                anime.SetTrigger("doPushUp");
            break;
            case 6:
                cam.enabled = true;
                v = character.transform.position;
                v.y = -0.60f;
                character.transform.position = v;
                anime.SetTrigger("doBackSquat");
            break;
        }
        if (num >= 9) {
            anime.SetTrigger("doStanding");
            PowerUp ();
        }
    }

    void PowerUp(){
        sound.StopBgm();
        float x =  score.getScore () / 1000;
        character.transform.localScale = new Vector3(20*3*x,20*3*x,20*3*x);
        Debug.Log("score"+x);
        if(x < 0.6f){
            sound.PlaySe("magic2");
        }else{
            sound.PlaySe("magic1");
        }
        Observable.Timer (TimeSpan.FromSeconds (2)).Subscribe (l => {
            GameEnd ();
        });
    }
    void GameEnd () {
        Debug.Log ("" + score.getScore ());
        Vector3 v = gaugeListTrans.localPosition;
        v.y = -110.00f;
        gaugeListTrans.localPosition = v;
        t.text = "Score:" + score.getScore ().ToString ("F2");
        gm.StateChange ();
    }
}