﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;

public class ResultGaugeAnime : MonoBehaviour {

    [SerializeField]
    private ResultGauge g;
    private Image image;
    

    // Start is called before the first frame update
    void Start () {
        image = GetComponent<Image>();
        
        float num = g.gaugeNum;
        float param = num / 100;
        image.fillAmount = param;
        image.color = UnityEngine.Color.HSVToRGB (0.5f+(-0.5f * param), 1, 1);
    }

}