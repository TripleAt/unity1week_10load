﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class ResultGauge : MonoBehaviour {
    private float seconds;
    [SerializeField]
    private TextMeshProUGUI t;
    public float gaugeNum;
    float gaugeMax;


    // Start is called before the first frame update
    void Start () {
        t.text = gaugeNum.ToString ("F2");
    }

}