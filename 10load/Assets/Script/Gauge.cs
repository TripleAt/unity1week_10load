﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class Gauge : MonoBehaviour {
    private float seconds;
    private TextMeshProUGUI t;
    float gaugeNum;
    float gaugeMax;

    public float getGaugeNum{get{return gaugeNum;}}

    // Start is called before the first frame update
    void Start () {
        gaugeMax = Random.Range(0.3f, 1.0f);
        t = GetComponent<TextMeshProUGUI> ();
        this.UpdateAsObservable ().Subscribe (_ => updateStream ());
    }

    public void gaugeReset(){
        seconds = 0;
        gaugeMax = Random.Range(0.3f, 1.0f);
    }

    private void updateStream () {
        seconds += Time.deltaTime;

        gaugeNum = (float) accelerationMove (0, 100, seconds, gaugeMax);
        if (seconds > gaugeMax) {
            seconds = 0;
        }
        if(gaugeNum > 100){
            gaugeNum = 100;
        }
        t.text = gaugeNum.ToString ("F2");
    }

    double accelerationMove (float start, float end, float time, float maxTime) {
        return start + (end - start) * ((time + 1) * time / 2.0) / ((maxTime + 1) * maxTime / 2.0);
    }

}