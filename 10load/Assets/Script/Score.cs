using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Zenject;


public class Score : MonoBehaviour {
    private List<float> scoreList;
    
    public void clearScore(){
        if(scoreList == null){
            scoreList = new List<float>();
            return;
        }
        scoreList.Clear();
    }
    public void addScore(float score){
        scoreList.Add(score);
    }

    public float getScore(){
        float ans = 0;
        foreach (var num in scoreList){
            ans += num;
        }
        return ans;
    }
}