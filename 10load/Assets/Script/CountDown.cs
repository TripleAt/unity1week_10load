﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class CountDown : MonoBehaviour {
    [SerializeField]
    private float seconds;
    private RectTransform rectTrans;
    private Graphic m_Graphics;
    private string str;
    [SerializeField]
    private TextMeshProUGUI t;
    // Start is called before the first frame update
    void Start () {
        seconds = 0;
        rectTrans = GetComponent<RectTransform> ();
        m_Graphics = GetComponent<Graphic>();
        t = GetComponent<TextMeshProUGUI>();
        this.UpdateAsObservable ().Subscribe (_ => updateStream ());
    }

    public void bigin(string s){
        seconds = 0;
        str = s;
        t.text = s;
        var color = m_Graphics.color;
        color.a = 1;
        m_Graphics.color = color;
        rectTrans.localScale = Vector3.one;
    }

    private void updateStream () {
        if (seconds < 1) {
            seconds += Time.deltaTime;
            float sclase = (float) decelerationMove (1, 7, 1, seconds);
            rectTrans.localScale = new Vector3 (sclase, sclase, sclase);

            var color = m_Graphics.color;
            color.a = (1-seconds);
            m_Graphics.color = color;
        }
    }

    private double decelerationMove (float start, float end, float maxTime, float time) {
        return start + (end - start) * ((maxTime * 2 - time + 1) * time / 2.0) / ((maxTime + 1) * maxTime / 2.0);
    }

}