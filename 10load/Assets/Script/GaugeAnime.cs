﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;

public class GaugeAnime : MonoBehaviour {

    [SerializeField]
    private Gauge g;
    private Image image;
    

    // Start is called before the first frame update
    void Start () {
        image = GetComponent<Image>();
        this.UpdateAsObservable ().Subscribe (_ => updateStream ());
    }

    private void updateStream () {
        float num = g.getGaugeNum;
        float param = num / 100;
        image.fillAmount = param;
        image.color = UnityEngine.Color.HSVToRGB (0.5f+(-0.5f * param), 1, 1);
    }

}